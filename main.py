from openai import OpenAI
from dotenv import load_dotenv
from typing import Dict, Any


load_dotenv()

client = OpenAI()

def upload_file(filename: str):
    if filename == "":
        return None
    file = client.files.create(file=open(filename, "rb"), purpose="fine-tune")
    return file

def create_fine_tuning(
    model: str,
    filename: str,
    fine_tuning_name: str,
    hyperparameters: Dict[str, Any],
    validation_file="",
):
    file = upload_file(filename)
    fine_tuned = client.fine_tuning.jobs.create(
        training_file=file.id, model=model, suffix=fine_tuning_name
    )
    return fine_tuned


if __name__ == "__main__":
    hyperparameters = {"n_epochs": 2, "batch_size": 8, "learning_rate_multiplier": 0.16}
    fine_tuning_object = create_fine_tuning(
        "gpt-3.5-turbo-0125", "dataset.jsonl", "kzm", hyperparameters
    )
    while True:
        completion = client.chat.completions.create(
            model=fine_tuning_object.id,
            messages=[{"role": "user", "content": "How far is mars from Earth?"}],
        )
        print(completion.choices[0].message)
